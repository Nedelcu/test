class ExtendedClock extends Clock {
    constructor(ext) {
      super(ext);
      let { precision = 1000 } = ext;
      this.precision = precision;
    }
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), this.precision);
    }
  };