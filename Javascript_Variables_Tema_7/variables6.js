//6.1
const nameSpaceShuttle = "Determination"; 
let speed = 17500; //miles per hour
let distToMars = 225000000; // distance to Mars in km
let distToMoon = 384400; // distance to Moon in km
const milesPerKilo = 0.621; // miles per km

//6.2
let milesToMars = distToMars * milesPerKilo; 
let hoursToMars = milesToMars / speed;
let daysToMars = hoursToMars / 24;

//6.3
alert (`${nameSpaceShuttle} will take ${daysToMars} days to reach Mars.`);

//6.4
let milesToMoon = distToMoon * milesPerKilo;
let hoursToMoon = milesToMoon / speed;
let daysToMoon = hoursToMoon / 24;

alert (`${nameSpaceShuttle} will take ${daysToMoon} days to reach Moon.`);