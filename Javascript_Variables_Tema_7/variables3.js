const myAge = 21;
let maxAge = 80;
let appleAmount = 2; // amount per day
let totalAmount = (appleAmount * 365) * (maxAge - myAge);

alert(`You will need ${totalAmount} to last until the ripe old age of ${maxAge}`);