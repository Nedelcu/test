const multiplayer = document.querySelector(".game-multiplayer");
const start = document.getElementById("start");
const btnBoard = document.querySelector(".changeBoard");
const containerStart = document.querySelector(".multiplayer-container");
const inputType = document.querySelector(".inputs-type")
let movesP1 = document.getElementById("moves-p1");
let c1 = document.getElementsByName("change")[0];
let c2 = document.getElementsByName("change")[1];
let kratosTurn = document.getElementById("kratos");
let board = document.querySelector(".board");
let showImageF = document.getElementById("zeustype");
let showImageS = document.getElementById("athenatype");
const checkS = document.getElementById("4x3");
const checkF = document.getElementById("3x3");
const winM = document.getElementById('winMessage');
let memory = document.getElementById("memory-message");

let obj = {};
obj.btn1_On = new Image();
btn1_On = "./images/musicOn.png";
obj.btn1_Off = new Image();
btn1_Off= "./images/mute.png";
function togglePlay() {
  let audioBackground = document.getElementsByTagName("audio")[0];
   
  if (audioBackground) {
    if (audioBackground.paused) {
      audioBackground.play();
       document.getElementById("btn").src = btn1_On;
    } else {
      audioBackground.pause();
        document.getElementById("btn").src = btn1_Off;
    }
  }
}


showImageS.addEventListener('click', function(){
    checkS.checked = true;
})
showImageF.addEventListener('click', function(){
    checkF.checked = true;
})


showImageF.addEventListener("mouseover", function () {
    let SingleShow = new openIt();
    SingleShow.showImageFirst();
})

showImageF.addEventListener('mouseout', function () {
    let SingleHide = new openIt;
    SingleHide.hideImg();
})

showImageS.addEventListener('mouseover', function () {
    let secondShow = new openIt();
    secondShow.showImageSecond();
})

showImageS.addEventListener('mouseout', function () {
    let secondHide = new openIt();
    secondHide.hideImg();
})

class openIt {
    showImageFirst() {
        document.getElementById('show-first-image').style.display = "block";
        document.getElementById('show-second-image').style.display = "none";
    }
    showImageSecond() {
        document.getElementById('show-first-image').style.display = "none";
        document.getElementById('show-second-image').style.display = "block";
    }
    hideImg() {
        document.getElementById('show-first-image').style.display = "none";
        document.getElementById('show-second-image').style.display = "none";
    }
}

const deck = document.querySelectorAll('.card');
deck.forEach(card => card.addEventListener('click', flipCard));
const timeValue = document.getElementById("time");

shuffleDeck(); // randomizes the board before each game

const displayScore1 = document.querySelector('#score1');

let score1 = 0;
let isFirstCard = false;
let first, second;
let isBoardClosed = false;
let p1Turn = true;
let seconds = 0,
    minutes = 0;
let moves1 = 0;
let moves2 = 0;



function startGame() {

    multiplayer.style.display = "block";
    containerStart.style.display = "none";
    inputType.style.display = "none";
    if (c1.checked) {
        for (let i = 0; i < 4; i++) {
            deck[i].classList.add("hide");
            deck[i].classList.add("resize");
            board.style.gridTemplateRows = "repeat(3, 1fr)";
        }
    }
    if (c2.checked) {
        for (let i = 0; i < 4; i++) {
            deck[i].classList.remove("hide");
        }
    }


}

/**
 * Shuffles the deck so that each game has a completely different board
 */
function shuffleDeck() {
    deck.forEach(card => {
        let randomIndex = Math.floor(Math.random() * 16);
        card.style.order = randomIndex;
    });

}
function flipCard() {
    if (isBoardClosed) return;
    if (this === first) return;
    this.classList.add('flip');
    if (!isFirstCard) {
        isFirstCard = true; //first click
        first = this; // 'this' = the element that has fired the event
        return
    }

    isFirstCard = false; //second click
    second = this;

    // if the second card has been chosen, check if they match
    checkMatch();

}

function checkMatch() {
    // movesCounter();
    if (p1Turn) {
        moves1++;
        movesP1.innerHTML = `Moves: ${moves1}`;
        console.log(moves1)
    }
    if (first.dataset.id == second.dataset.id) {
        //so cards cannot be clicked again
        first.removeEventListener('click', flipCard);
        second.removeEventListener('click', flipCard);
        first.classList.add('cursor');
        second.classList.add('cursor');
        resetBoard();
        if (p1Turn) {
            score1 += 1;
            displayScore1.textContent = score1.toString();
        }
        checkGameOver();

    }
    else {
        //if the cards are not a match then turn them over again
        isBoardClosed = true;
        setTimeout(() => {
            first.classList.remove('flip');
            second.classList.remove('flip');
            isBoardClosed = false;
            resetBoard();
        }, 1000);

        // p2Turn = true;

    }
    // if (p1Turn) {
    //     p1Turn = false;
    // }
    // else if (!p1Turn) {
    //     p1Turn = true;
    // }
}

/**
 * Prevents more than 2 cards being flipped over at once since it is against the rules
 */
function resetBoard() {
    first = null;
    second = null;
    isFirstCard = false;
    isBoardClosed = false;
}


function restart(){
    for (let i=0; i<deck.length; i++){
        if(deck[i].classList.contains("hide"))
        {
            deck[i].classList.remove("hide");
        }
        deck[i].classList.remove("flip");
        deck[i].classList.remove("cursor"); 
    }
    
    shuffleDeck();
    deck.forEach(card => card.addEventListener('click', flipCard));
    moves1 = 0;
    movesP1.innerHTML = `Moves: ${moves1}`;
    score1 = 0;
    displayScore1.textContent = score1.toString();
    p1Turn = true;
    winM.style.display = "none";
    board.style.display = "grid";
    multiplayer.style.display = "block";
    containerStart.style.display = "none";
    inputType.style.display = "none";
    memory.style.display = "block";
    if (c1.checked) {
        for (let i = 0; i < 4; i++) {
            deck[i].classList.add("hide");
            deck[i].classList.add("resize");
            board.style.gridTemplateRows = "repeat(3, 1fr)";
        }
    }
    if (c2.checked) {
        for (let i = 0; i < 4; i++) {
            deck[i].classList.remove("hide");
        }
    }
}

function exit() {
    multiplayer.style.display = "none";
    containerStart.style.display = "flex";
    inputType.style.display = "flex";
};

function checkGameOver() { // game is over if either player gets 5/3 points
    if (c2.checked) {
        if (score1 === 8) {
            board.style.display = "none";
            winM.style.display = "block"
            memory.style.display = "none";
        }
    }
    else {
        if (score1 === 6) {
            board.style.display = "none";
            winM.style.display = "block"
            memory.style.display = "none";
        }
    }
}