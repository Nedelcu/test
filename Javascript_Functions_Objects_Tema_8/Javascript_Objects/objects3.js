let movie = {
    title: 'Puff the Magic Dragon',
    duration: 30,
    stars: ['Puff', 'Jackie', 'Living Sneezes']
};

function printMovie(movieInfo) {
    console.log(`${movieInfo.title} lasts for ${movieInfo.duration} minutes. Stars ${movieInfo.stars.join(', ')}.`);
};

printMovie(movie);
