//Exercitiul 3 - The Lifetime Supply Calculator

function calculateSupply (age, amountDay) {
    const maxAge = 85;
    ageDifference = maxAge - age;
    amountDay = Math.round(amountDay);
    amountTotal = (amountDay * 365) * ageDifference;
    console.log(`You will need ${amountTotal} to last you until the ripe old age of ${maxAge}`)
}

calculateSupply(21, 5);
calculateSupply(34, 10);
calculateSupply(55, 4);