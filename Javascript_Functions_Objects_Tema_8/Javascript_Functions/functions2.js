//Exercitiul 2 - The Puppy Age Calculator

function calculateDogAge(puppyAge, myAge) {
    console.log (`Your doggie is ${puppyAge*7} years old in human years!`)
    console.log (`My age is ${myAge/7} years old in dog years!`)
}

calculateDogAge (3, 15);
calculateDogAge (5, 21);
calculateDogAge (8, 34);