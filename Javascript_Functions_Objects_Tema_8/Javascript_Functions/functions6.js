// Exercitiul 6 - The Calculator

function squareNumber (num1) {
    square_num1 = num1 * num1;
    console.log (`The result of squaring the number ${num1} is ${square_num1}`);

    return square_num1;
}

squareNumber (6);

function halfNumber(num2) {
    divide_num2 = num2 / 2;
    console.log (`Half of ${num2} is ${divide_num2}`);

    return divide_num2;
}

halfNumber(5)

function percentOf(percentNum3, percentNum4) {
    percentOfNumbers = (percentNum3/percentNum4) * 100;
    console.log (`${percentNum3} is ${percentOfNumbers} % of ${percentNum4}`);

    return percentOfNumbers;
}

percentOf(5,10);

function areaOfCircle(radius) {
    area = (Math.PI * Math.pow(radius, 2)).toFixed(2);
    console.log(`The area for a circle with radius ${radius} is ${area}`)

    return area;
}

areaOfCircle(2);

function fullTypes(lastNum) {
    divide_num2 = halfNumber(lastNum);
    square_num1 = squareNumber(lastNum);
    area = areaOfCircle(lastNum);
    percentOfNumbers = percentOf(area, square_num1);
}

fullTypes(5);