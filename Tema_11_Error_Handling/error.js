
function read(json) {
    let user = JSON.parse(json);
    if(!user.age && !user.name) { // Verificam daca exista si nume si varsta
        throw new SyntaxError("no name and no age") // Daca nu exista niciun "obiect", o sa arate eroarea respectiva
    }
    if(!user.name) { // Daca nu exista "obiectul" de nume
        throw new SyntaxError("no name"); // Trimite eroarea respectiva
    }

    if(!user.age) { // Daca nu exista "obiectul" de age
        throw new SyntaxError("no age"); // Trimite eroarea aferenta
    }

    return user;
}