class ValidatorError extends Error {
    constructor(message){
    super(message);
    this.name = "ValidatorError";
    }

    validateInfo(user){
    if(typeof (user.name) != "string") { // Daca numele nu este de tip string
        throw new ValidatorError("wrong type of name"); // Trimite eroarea
    }
    if(typeof(user.age) != "number") { // Daca varsta nu este de tip numar
        throw new ValidatorError("Wrong type of age"); // Trimite eroarea
    }
}
}

function customValidate(json) {
    let firstUser = JSON.parse(json);

    let validation = new ValidatorError("");
    validation.validateInfo(firstUser);
}