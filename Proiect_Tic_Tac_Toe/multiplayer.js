let c1 = document.getElementsByName('characP1');
let c2 = document.getElementsByName('characP2');
let containerDiss = document.getElementById('thiswilldiss');
let errorMessage = document.getElementById('message');



let char1;
let char2;

function players(name, val) {
    this.name = name;
    this.val = val;
}

for (i = 0; i < c1.length; i++) {
    c1[i].addEventListener('change', function () {
        if (this.checked) {
            this.value == 'X' ? c2[1].checked = true : c2[0].checked = true;
            char1 = this.value;
            char2 = this.value == 'X' ? 'O' : 'X';
        }
    });
}
for (i = 0; i < c2.length; i++) {
    c2[i].addEventListener('change', function () {
        if (this.checked) {
            this.value == 'X' ? c1[1].checked = true : c1[0].checked = true;
            char2 = this.value;
            char1 = this.value == 'X' ? 'O' : 'X';
        }
    });
}
function StartAction() {
    let firstPlayerName
    let secondPlayerName
    if (char1 == 'X') {
        firstPlayerName = document.getElementById('player1').value;
        secondPlayerName = document.getElementById('player2').value;
    }
    else {
        secondPlayerName = document.getElementById('player1').value;
        firstPlayerName = document.getElementById('player2').value;

    }


    firstPlayer = new players(firstPlayerName, char1);
    secondPlayer = new players(secondPlayerName, char2);
    containerDiss.classList.add('hide');
    console.log(firstPlayer.name)
    console.log(secondPlayer.name)


    if (firstPlayer.name && firstPlayer.val && secondPlayer.name && secondPlayer.val) {
        let joc = document.getElementById('joc');
        joc.id = 'joc-start';
        console.log('MERGE!');
        let isGameActive = true;
        let currentPlayer = 'X';
        const playerDisplay = document.querySelector('.display-player');
        playerDisplay.innerText = `${firstPlayer.name}`;
        console.log(playerDisplay.innerText);
        window.addEventListener('click', () => {
            const tiles = Array.from(document.querySelectorAll('.tile'));
            const resetButton = document.querySelector('#reset');
            const announcer = document.querySelector('.announcer');
            let board = ['', '', '', '', '', '', '', '', ''];

            const PLAYERX_WON = 'PLAYERX_WON';
            const PLAYERO_WON = 'PLAYERO_WON';
            const TIE = 'TIE';


            /*
                Indexes within the board
                [0] [1] [2]
                [3] [4] [5]
                [6] [7] [8]
            */

            const winningConditions = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8],
                [0, 3, 6],
                [1, 4, 7],
                [2, 5, 8],
                [0, 4, 8],
                [2, 4, 6]
            ];

            function handleResultValidation() {

                let roundWon = false;
                for (let i = 0; i <= 7; i++) {
                    const winCondition = winningConditions[i];
                    const a = board[winCondition[0]];
                    const b = board[winCondition[1]];
                    const c = board[winCondition[2]];
                    if (a === '' || b === '' || c === '') {
                        continue;
                    }
                    if (a === b && b === c) {
                        roundWon = true;
                        break;
                    }
                }

                if (roundWon) {
                    announce(currentPlayer === 'X' ? PLAYERX_WON : PLAYERO_WON);
                    isGameActive = false;
                    return;
                }

                if (!board.includes(''))
                    announce(TIE);
            }

            const announce = (type) => {

                switch (type) {
                    case PLAYERO_WON:
                        announcer.innerText = `Player ${secondPlayer.name} Won!!!`;
                        console.log(secondPlayer);
                        break;
                    case PLAYERX_WON:
                        announcer.innerText = `Player ${firstPlayer.name} Won`;
                        console.log(firstPlayer);
                        break;
                    case TIE:
                        announcer.innerText = 'Tie';
                }
                announcer.classList.remove('hide');
            };

            const isValidAction = (tile) => {
                if (tile.innerHTML === 'X' || tile.innerText === 'O') {
                    return false;
                }

                return true;
            };
            console.log("merge2");
            const updateBoard = (index) => {
                board[index] = currentPlayer;
            }
            const changePlayer = () => {
                playerDisplay.classList.remove(`player${currentPlayer}`);
                currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
                if (currentPlayer === 'X') {
                    playerDisplay.innerText = `${firstPlayer.name}`;
                }
                else {
                    playerDisplay.innerText = `${secondPlayer.name}`;
                }
                playerDisplay.classList.add(`player${currentPlayer}`);
            }

            const userAction = (tile, index) => {
                if (isValidAction(tile) && isGameActive) {
                    tile.innerText = currentPlayer;
                    tile.classList.add(`player${currentPlayer}`);
                    updateBoard(index);
                    handleResultValidation();
                    changePlayer();
                }
            }



            tiles.forEach((tile, index) => {
                tile.addEventListener('click', () => userAction(tile, index));
            });

            //resetButton.addEventListener('click', location.reload());
        });
        console.log("merge3");


    }
    else {
        errorMessage.innerHTML = ("Make sure you have completed the name inputs and chosen your character, ok? Now go back!")
    }
};