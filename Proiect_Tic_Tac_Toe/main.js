let buttonSingleplayer = document.getElementById('playSingleplayer');
let buttonMultiplayer = document.getElementById('playMultiplayer');

buttonSingleplayer.addEventListener('mouseover', function(){
    let singleShow = new openIt();
    singleShow.showImgS();
});

buttonSingleplayer.addEventListener('mouseout', function(){
    let singleHide = new openIt();
    singleHide.hideImg();
});

buttonMultiplayer.addEventListener('mouseover', function(){
    let multiShow = new openIt();
    multiShow.showImgM();
});

buttonMultiplayer.addEventListener('mouseout', function(){
    let multiHide = new openIt();
    multiHide.hideImg();
});

class openIt {
    showImgS(){
        document.getElementById('show-image-singleplayer').style.display="block";
        document.getElementById('show-image-multiplayer').style.display="none";
    }

    showImgM(){
        document.getElementById('show-image-singleplayer').style.display="none";
        document.getElementById('show-image-multiplayer').style.display="block";
    }

    hideImg(){
        document.getElementById('show-image-singleplayer').style.display="none";
        document.getElementById('show-image-multiplayer').style.display="none";
    }
}