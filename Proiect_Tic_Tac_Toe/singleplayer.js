

let hideMenu = document.getElementById("hide-menu");
let c1 = document.getElementById('characP1');
let c2 = document.getElementById('characP2');
console.log(c1)
let errorMessage = document.getElementById('message');


function StartActionSg(){

    let player1=document.getElementById('jucator1').value;
    if (player1!==''){
        errorMessage.style.display="none";
        let joc=document.getElementById('joc');
        joc.id='joc-start';
        hideMenu.classList.add('hide');
        function init() {
            const playerTitle = document.querySelector(".playerTitle");
            const rematchBtn = document.querySelector(".rematch");
            const RestartBtn= document.querySelector(".Restart");
            const items = document.querySelectorAll(".item");
            const gridArray = Array.from(items);
            let tracking = [1, 2, 3, 4, 5, 6, 7, 8, 9];


            items.forEach((item) =>
                item.addEventListener("click", (e) => {
                    // Player Move
                    console.log(items);
                    console.log(item);
                    const index = gridArray.indexOf(e.target);
                    if (
                        items[index].classList.contains("playerX") ||
                        items[index].classList.contains("computer")
                    ) {
                        return;
                    }

                    console.log(items);
                    console.log(item);
                    if(c1.checked == true)
                        items[index].classList.add("playerX");
                    else {
                        items[index].classList.add("computer");
                    }
                    
                    const spliceNr = tracking.indexOf(index + 1);
                    // slicing out the move from the tracking list
                    tracking.splice(spliceNr, 1);

                    // win check for player
                    if (c1.checked && winCheck("playerX", items)) {

                        playerTitle.innerHTML = `Player ${player1} Win`;
                        document.body.classList.add("over");
                        return;
                    }

                    if (c2.checked && winCheck("computer", items)) {
                        playerTitle.innerHTML = `Player ${player1} Win`;
                        document.body.classList.add("over");
                        return;
                    }

                    // check for draw
                    if (tracking.length === 0) {
                        playerTitle.innerHTML = "It's Draw. Try again?";
                        document.body.classList.add("over");
                        console.log("Nothing Left");
                        return;
                    }

                    // Computer Move
                    const random = Math.floor(Math.random() * tracking.length);
                    const computerIndex = tracking[random];
                    console.log
                    if(c1.checked == true)
                        items[computerIndex - 1].classList.add("computer");
                    else {
                        items[computerIndex - 1].classList.add("playerX");
                    }
                
                    

                    // Splicing out the move from the tracking list
                    tracking.splice(random, 1);

                    // win check for the computer
                    if (c1.checked && winCheck("computer", items)) {
                        playerTitle.innerHTML = "Computer Win";
                        document.body.classList.add("over");
                        return;
                    }

                    if(c2.checked && winCheck("playerX", items)) {
                        playerTitle.innerHTML = "Computer Win";
                        document.body.classList.add("over");
                        return;
                    }
                })
            );

            // rematch reload event
            rematchBtn.addEventListener("click", () => {
                location.reload();
            });
            RestartBtn.addEventListener("click",() => {
                resetTable();
            });

            function resetTable(){
                for(let i=0; i<items.length; i++){
                console.log(items[i]);
                items[i].classList.remove("playerX");
                items[i].classList.remove("computer");
                spliceNr = 0;
                tracking = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                playerTitle.innerHTML = "Tic Tac Toe : Single Player Mode";
                document.body.classList.remove("over");
                

                
            }
        }



// win check function
        function winCheck(player, items) {
            // let allItems = items;
            function check(pos1, pos2, pos3) {
                console.log(items);
                if (
                    items[pos1].classList.contains(player) &
                    items[pos2].classList.contains(player) &
                    items[pos3].classList.contains(player)
                ) {
                    return true;
                } else {
                    return false;
                }
            }

            if (check(0, 3, 6)) return true;
            else if (check(1, 4, 7)) return true;
            else if (check(2, 5, 8)) return true;
            else if (check(0, 1, 2)) return true;
            else if (check(3, 4, 5)) return true;
            else if (check(6, 7, 8)) return true;
            else if (check(0, 4, 8)) return true;
            else if (check(2, 4, 6)) return true;
        }
    }
// initializing the game
    init();
    }
    else{
        errorMessage.innerText = ("Make sure you have completed the name input and chosen your character!");
        console.log(errorMessage);
    }
}

